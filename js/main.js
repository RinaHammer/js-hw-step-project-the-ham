//---------tabs
const tabs = document.querySelector(".our-services-tabs");
function showText() {}
function showText() {
  const tabs = document.querySelector(".our-services-tabs");
  tabs.addEventListener("click", (e) => {
    if (e.target.tagName === "LI") {
      let active = document.querySelector(".our-services-tabs-item.active");
      active.classList.remove("active");
      e.target.classList.add("active");
    } else if (e.target.tagName !== "LI") {
      return;
    }
    const text = document.querySelectorAll(".our-services-content-item");
    text.forEach((el) => {
      if (e.target.dataset.text === el.dataset.text) {
        let textActive = document.querySelector(".show");
        textActive.classList.remove("show");
        el.classList.add("show");
      }
    });
  });
}
showText();
// ---------filter photo
const filterBtns = document.querySelector(".filter-buttons");
filterBtns.addEventListener("click", (e) => {
  const imgs = document.querySelectorAll(".work-item");
  if (e.target.tagName === "UL") {
    return;
  }
  if (e.target.tagName === "LI") {
    let active = document.querySelector(".filter-btn.active");
    active.classList.remove("active");
    e.target.classList.add("active");
  }
  const category = e.target.dataset.category;
  imgs.forEach((item) => {
    if (category !== "All") {
      item.dataset.category !== category
        ? item.classList.add("hide")
        : item.classList.remove("hide");
    } else {
      item.classList.remove("hide");
    }
  });
});
//----------add svg
const icons = document.querySelectorAll(".work-img-info");
icons.forEach((item) => {
  item.insertAdjacentHTML(
    "afterbegin",
    `<div class="icon">
    <a class="figure-chain" href="#">
        <svg class="ellipse" width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB"/>
            </svg>
        <svg class="chain" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path  class="chain-path" fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg>
                
    </a>        
    <a class="figure-square" href="#">   
        <svg class="ellipse" width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB"/>
                </svg>
        <svg class="square" width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect class="square-path" width="12" height="11" fill="#1FDAB5"/>
                    </svg>
                    
                    
                                    
        <!-- <img class="icon" src="./image/logo/icon.png" alt=""> -->
    </a>
</div>`
  );
});

//----------add photo
const webDesign = [
  "./image/add photo/Web Design.jpg",
  "./image/add photo/Web Design1.jpg",
  "./image/add photo/Web Design2.jpg",
];
const graphicDesign = [
  "./image/add photo/Graphic Design.jpg",
  "./image/add photo/Graphic Design2.jpg",
  "./image/add photo/Graphic Design3.jpg",
];
const landingPage = [
  "./image/add photo/Landing Pages.jpg",
  "./image/add photo/Landing Pages1.jpg",
  "./image/add photo/Landing Pages2.jpg",
];
const wordpress = [
  "./image/add photo/Wordpress.jpg",
  "./image/add photo/Wordpress1.jpg",
  "./image/add photo/Wordpress2.jpg",
];
let load = document.querySelector(".load-more-btn");
load.addEventListener("click", showAnimation);

function addPhoto(category, arr) {
  arr.forEach((item) => {
    const ul = document.querySelector(".work-images");
    const li = document.querySelectorAll(".work-item");
    let liNew;
    li.forEach((elem) => {
      if (elem.dataset.category === category) liNew = elem.cloneNode(true);
    });
    let liImg = liNew.querySelector("img");
    liImg.src = item;
    liNew.dataset.category = category;
    ul.append(liNew);
  });
}
function showAnimation() {
  document.querySelector(".gooey").style.display = "block";
  load.remove();
  setTimeout(showPhoto, 2000);
  setTimeout(() => {
    document.querySelector(".gooey").style.display = "none";
  }, 2000);
}
function showPhoto() {
  addPhoto("Graphic Design", graphicDesign);
  addPhoto("Web Design", webDesign);
  addPhoto("Landing Pages", landingPage);
  addPhoto("Wordpress", wordpress);
}
//--------buttons next/prev
const prev = document.querySelector(".btn-prev");
const next = document.querySelector(".btn-next");
const images = document.querySelectorAll(".slider-item");
const totalImages = images.length;
const authors = document.querySelector(".authors");
const authorsLi = document.querySelectorAll(".feedback-text");
const review = document.querySelector(".feedback-review");
let offset = 0;
let index = 0;

let i = 0;
Array.from(images).forEach((el) => (el.dataset.number = `${(i += 1)}`));
let n = 0;
Array.from(authorsLi).forEach((el) => (el.dataset.number = `${(n += 1)}`));

prev.addEventListener("click", () => {
  prevImage();
  pushPrev();
});
next.addEventListener("click", () => {
  nextImage();
  pushNext();
});
function nextImage() {
  let activePhoto = document.querySelector(".active-photo");
  activePhoto.classList.remove("active-photo");
  let number = activePhoto.dataset.number;
  number = +number + 1;
  if (number === 5) {
    number = 1;
  }
  for (let i = 0; i < totalImages; i++) {
    images[i].dataset.number != number
      ? images[i].classList.remove("active-photo")
      : images[i].classList.add("active-photo");
  }
}
function prevImage() {
  let activePhoto = document.querySelector(".active-photo");
  activePhoto.classList.remove("active-photo");
  let number = activePhoto.dataset.number;
  number = +number - 1;
  if (number === 0) {
    number = 4;
  }
  for (let i = 0; i < totalImages; i++) {
    images[i].dataset.number != number
      ? images[i].classList.remove("active-photo")
      : images[i].classList.add("active-photo");
  }
}
function pushNext() {
  let position = parseFloat(review.style.left);
  let number = document.querySelector(".active-photo").dataset.number;
  if (!position) {
    offset += 1160;
    if (offset > (+number - 1) * 1160) {
      offset = 0;
    }
    review.style.left = -offset + "px";
  } else {
    offset = -position;
    offset += 1160;
    if (offset > (+number - 1) * 1160) {
      offset = 0;
    }
    review.style.left = -offset + "px";
  }
  review.style.left = -offset + "px";
}
function pushPrev() {
  let position = parseFloat(review.style.left);
  let number = document.querySelector(".active-photo").dataset.number;
  if (!position) {
    offset -= 1160;
    if (offset < 0) {
      offset = (+number - 1) * 1160;
    }
    review.style.left = -offset + "px";
  } else {
    offset = -position;
    offset -= 1160;
    if (offset < 0) {
      offset = (+number - 1) * 1160;
    }
    review.style.left = -offset + "px";
  }
}
//-----------active review

authors.addEventListener("click", (e) => {
  if (e.target.tagName === "UL") {
    return;
  }
  if (e.target.tagName === "IMG") {
    let activePhoto = document.querySelector(".active-photo");
    activePhoto.classList.remove("active-photo");
    let li = e.target.closest("li");
    li.classList.add("active-photo");
    const number = e.target.closest("li").dataset.number;
    chooseFoto(number);
  }
});
function chooseFoto(number) {
  if (number == 1) {
    offset = 0 + "px";
  } else {
    offset = -1160 * (+number - 1) + "px";
  }
  authorsLi.forEach((item) => {
    if ((number = item.dataset.number)) review.style.left = offset;
  });
}
